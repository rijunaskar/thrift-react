import React, { Component } from 'react'
import withStyles from '@material-ui/core/styles/withStyles';
import { Typography } from '@material-ui/core';

// MUI Stuff

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';


const styles = {
    card: {
        display: 'flex'
    }
}
class Scream extends Component {
    render() {
        const { classes, scream : { body, createdAt, userImage, userHandle, screamId} } = this.props;

        return (
            <div>
                <Card>
                <CardMedia
                    image = {userImage}
                    title = "Profile Image"/>
                    <CardContent>
                        <Typography variant="h5">{userHandle.toString()}</Typography>
                        <Typography variant="body2" color="textSecondary">{createdAt.toString()}</Typography>
                        <Typography variant="body1">{body.toString()}</Typography>
                    </CardContent>
                </Card>
            </div>
        )
    }
}

export default withStyles(styles)(Scream)
